FROM  maven:3.6.0-jdk-11-slim AS build
COPY ./src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package -DskipTests

FROM adoptopenjdk:11-jre-hotspot
ARG JAR_FILE=*.jar
COPY --from=build /home/app/target/${JAR_FILE} application.jar
# COPY target/${JAR_FILE} application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]
