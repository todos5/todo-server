package com.yoki.todos.repo;

import com.yoki.todos.data.Todo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "todos", path = "todos")
public interface TodoRepo extends JpaRepository<Todo, Long> {
  List<Todo> findByCompleted(@Param("status") Boolean status);
}
