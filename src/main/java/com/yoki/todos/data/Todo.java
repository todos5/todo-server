package com.yoki.todos.data;

import java.util.Date;
import javax.persistence.*;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@Entity
@Table(name = "todos")
public class Todo extends Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NonNull
  private String title;

  private Boolean completed = false;

  public Todo(Date createdAt, Date updatedAt, String lastUpdatedBy, Long id, @NonNull String title, Boolean completed) {
    super(createdAt, updatedAt, lastUpdatedBy);
    this.id = id;
    this.title = title;
    this.completed = completed;
  }

  public Todo(@NonNull String title, String author) {
    super(author);
    this.title = title;
  }
}
